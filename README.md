# Binance spot agent project

This module installed near binance exchange (Tokyo) it's parse  spot data and send it to main application (Spider Commander)  
This project uses Quarkus, Undertow, Binanace spot client

Docker:
docker pull lbom/crypto:binanceagent

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```
./mvnw quarkus:dev
```

## Packaging and running the application

The application can be packaged using `./mvnw package`.
It produces the `binancespotagent-1.0-SNAPSHOT-runner.jar` file in the `/target` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/lib` directory.

The application is now runnable using `java -jar target/binancespotagent-1.0-SNAPSHOT-runner.jar`.