package com.crypto;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.BinanceApiWebSocketClient;
import lombok.Getter;

import javax.inject.Singleton;
import java.beans.JavaBean;

@JavaBean
@Singleton
public class Exchange {

    public static final String NAME = "binance";
    public static final String MARKET = "spot";
    public static final int QUOTES_MAX_DEPTH = 5000;
    private static final String API_KEY = "key";
    private static final String SECRET_KEY = "key";

    @Getter
    private final BinanceApiWebSocketClient webSocketSpotClient;
    @Getter
    private final BinanceApiRestClient restSpotClient;

    public Exchange() {

        webSocketSpotClient = BinanceApiClientFactory.newInstance().newWebSocketClient();
        restSpotClient = BinanceApiClientFactory.newInstance(API_KEY, SECRET_KEY).newRestClient();
    }
}