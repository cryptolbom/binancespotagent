package com.crypto.services;

import org.slf4j.Logger;

import javax.websocket.Session;
import java.io.Closeable;
import java.io.IOException;
import java.util.Map;

public class Streaming {

    static void sendMessage(String key, String message, Map<String, Session> sessions,
                            Logger log, Map<String, Closeable> channels) {

        for (Session s : sessions.values()) {
            if (s.getUserProperties().containsKey(key)) {
                s.getAsyncRemote().sendText(message, result -> {
                    if (!result.isOK()) {
                        log.error("Error sending message, session id: {}, message: {}" ,
                                s.getId(), result.getException().getMessage());

                        s.getUserProperties().remove(key);
                    }
                });
            }
        }
    }

    static void checkChannels(String key, Map<String, Session> sessions, Logger log, Map<String, Closeable> channels) {
        try {
            boolean isAnySubscribed = sessions.values().stream()
                    .map(Session::getUserProperties)
                    .anyMatch(prop -> prop.containsKey(key));
            if (!isAnySubscribed && channels.containsKey(key)) {
                channels.get(key).close();
                channels.remove(key);
            }
            sessions.values().removeIf(value -> value.getUserProperties().isEmpty());
        } catch (IOException e) {
            log.error("Error while closing channel:" + e.getMessage());
        }
    }
}