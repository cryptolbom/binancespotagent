package com.crypto.services;

import com.binance.api.client.domain.event.DepthEvent;
import com.binance.api.client.exception.BinanceApiException;
import com.crypto.Exchange;
import com.crypto.messages.QuoteDTO;
import lombok.extern.slf4j.Slf4j;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import java.io.Closeable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@ApplicationScoped
@ServerEndpoint("/quotes/{pair}")
public class QuotesStreaming {

    @Inject
    Exchange exchange;

    Map<String, Closeable> channels = new ConcurrentHashMap<>();
    Map<String, Session> sessions = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("pair") String pair) {

        session.getUserProperties().put(pair, "");
        sessions.put(session.getId(), session);
        if (!channels.containsKey(pair)) {
            try {
                channels.put(pair, exchange.getWebSocketSpotClient().onDepthEvent(pair, this::broadcast));
            } catch (BinanceApiException e) {
                log.error("Error connection via binance client {}", e.getMessage());
                onError(session, e);
            }
        }
    }

    @OnClose
    public void onClose(Session session, @PathParam("pair") String pair) {

        session.getUserProperties().remove(pair);
        Streaming.checkChannels(pair, sessions, log, channels);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {

        session.getAsyncRemote().sendObject(throwable.getMessage(), result ->  {
            if (result.getException() != null) {
                log.error("Unable to send error message: {}" , result.getException().getMessage());
            }
        });
    }

    private void broadcast(DepthEvent event) {

        String pair = event.getSymbol().toLowerCase();
        var asks = new HashMap<BigDecimal, BigDecimal>();
        var bids = new HashMap<BigDecimal, BigDecimal>();

        event.getAsks().forEach(v -> asks.put(new BigDecimal(v.getPrice()), new BigDecimal(v.getQty())));
        event.getBids().forEach(v -> bids.put(new BigDecimal(v.getPrice()), new BigDecimal(v.getQty())));

        String message = QuoteDTO.builder()
                .exchange(Exchange.NAME)
                .market(Exchange.MARKET)
                .pair(pair)
                .asks(asks)
                .bids(bids)
                .timestamp(event.getEventTime())
                .build().toString();

        Streaming.sendMessage(pair, message, sessions, log, channels);
    }

    @OnMessage
    public void onMessage(String message) {
        log.info(message);
    }
}