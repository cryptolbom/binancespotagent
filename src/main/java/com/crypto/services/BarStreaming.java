package com.crypto.services;

import com.binance.api.client.domain.event.CandlestickEvent;
import com.binance.api.client.domain.market.CandlestickInterval;
import com.binance.api.client.exception.BinanceApiException;
import com.crypto.Exchange;
import com.crypto.messages.BarDTO;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.Closeable;
import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@ApplicationScoped
@ServerEndpoint("/bars/{pair}/{interval}")
public class BarStreaming {

    @Inject
    Exchange exchange;

    ConcurrentHashMap<String, Closeable> channels = new ConcurrentHashMap<>();
    ConcurrentHashMap<String, Session> sessions = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("pair") String pair, @PathParam("interval") String interval) {

        String key  = pair + interval;
        session.getUserProperties().put(key, interval);
        sessions.put(session.getId(), session);
        if (!channels.containsKey(pair)) {
            try {
                channels.put(pair, exchange.getWebSocketSpotClient().onCandlestickEvent(pair,
                        CandlestickInterval.valueOfLabel(interval), this::broadcast));
            } catch (BinanceApiException e) {
                log.error("Error connection via binance client {}", e.getMessage());
                onError(session, e);
            }
        }
    }

    @OnClose
    public void onClose(Session session, @PathParam("pair") String pair, @PathParam("interval") String interval) {

        String key  = pair + interval;
        session.getUserProperties().remove(key);
        Streaming.checkChannels(key, sessions, log, channels);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {

        session.getAsyncRemote().sendObject(throwable.getMessage(), result ->  {
            if (result.getException() != null) {
                log.error("Unable to send error message: {}" , result.getException().getMessage());
            }
        });
    }

    private void broadcast(CandlestickEvent event) {

        if (!event.getBarFinal()) return;

        String pair = event.getSymbol().toLowerCase();
        String key = pair + event.getIntervalId();

        var volume = new BigDecimal(event.getVolume());
        var buyVolume = new BigDecimal(event.getTakerBuyBaseAssetVolume());
        var sellVolume = volume.subtract(buyVolume);

        var costVolume = new BigDecimal(event.getQuoteAssetVolume());
        var costBuyVolume = new BigDecimal(event.getTakerBuyQuoteAssetVolume());
        var costSellVolume = costVolume.subtract(costBuyVolume);

        String message = BarDTO.builder()
                .exchange(Exchange.NAME)
                .market(Exchange.MARKET)
                .pair(event.getSymbol().toLowerCase())
                .period(event.getIntervalId())
                .baseVolume(new BigDecimal(event.getVolume()))
                .costVolume(costVolume)
                .baseBuyVolume(buyVolume)
                .costBuyVolume(costBuyVolume)
                .baseSellVolume(sellVolume)
                .costSellVolume(costSellVolume)
                .openTime(event.getOpenTime())
                .closeTime(event.getCloseTime())
                .open(new BigDecimal(event.getOpen()))
                .close(new BigDecimal(event.getClose()))
                .high(new BigDecimal(event.getHigh()))
                .low(new BigDecimal(event.getLow()))
                .tradesCount(event.getNumberOfTrades())
                .build().toString();

        Streaming.sendMessage(key, message, sessions, log, channels);
    }

    @OnMessage
    public void onMessage(String message) {
        log.info(message);
    }
}