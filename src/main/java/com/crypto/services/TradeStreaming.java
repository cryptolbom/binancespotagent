package com.crypto.services;

import com.binance.api.client.domain.event.AggTradeEvent;
import com.binance.api.client.exception.BinanceApiException;
import com.crypto.Exchange;
import com.crypto.messages.TradeDTO;
import lombok.extern.slf4j.Slf4j;

import java.io.Closeable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@Slf4j
@ApplicationScoped
@ServerEndpoint("/trades/{pair}")
public class TradeStreaming {

    @Inject
    Exchange exchange;

    Map<String, Closeable> channels = new ConcurrentHashMap<>();
    Map<String, Session> sessions = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("pair") String pair) {

        session.getUserProperties().put(pair, "");
        sessions.put(session.getId(), session);
        if (!channels.containsKey(pair)) {
            try {
                channels.put(pair, exchange.getWebSocketSpotClient().onAggTradeEvent(pair, this::broadcast));
            } catch (BinanceApiException e) {
                log.error("Error connection via binance client {}", e.getMessage());
                onError(session, e);
            }
        }
    }

    @OnClose
    public void onClose(Session session, @PathParam("pair") String pair) {

        session.getUserProperties().remove(pair);
        Streaming.checkChannels(pair, sessions, log, channels);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {

        session.getAsyncRemote().sendObject(throwable.getMessage(), result ->  {
            if (result.getException() != null) {
                log.error("Unable to send error message: {}" , result.getException().getMessage());
            }
        });
    }

    private void broadcast(AggTradeEvent event) {

        String pair = event.getSymbol().toLowerCase();
        var price = new BigDecimal(event.getPrice());
        var volume = new BigDecimal(event.getQuantity());
        var costVolume = price.multiply(volume, new MathContext(8, RoundingMode.HALF_DOWN));

        String message = TradeDTO.builder()
                .exchange(Exchange.NAME)
                .market(Exchange.MARKET)
                .pair(pair)
                .id(event.getAggregatedTradeId())
                .buy(event.isBuyerMaker())
                .price(price)
                .volume(volume)
                .costVolume(costVolume)
                .timestamp(event.getEventTime())
                .build().toString();

        Streaming.sendMessage(pair, message, sessions, log, channels);
    }

    @OnMessage
    public void onMessage(String message) {
        log.info(message);
    }
}