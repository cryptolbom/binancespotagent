package com.crypto;

import com.crypto.messages.QuoteDTO;
import com.crypto.testutils.JWTToken;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.mapper.ObjectMapperType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.Set;

import static io.restassured.RestAssured.given;


@QuarkusTest
public class QuotesRestTest {

    private String token;

    @BeforeEach
    public void generateToken() {
        token = JWTToken.newToken("binance");
    }

    @Test
    public void RestQuoteSnapAuthorization() {

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "btcusdt")
                .get("rest/quotes/snap")
                .then()
                .statusCode(200);
    }

    @Test
    public void RestQuoteSnapAuthorizationFailing() {

        given()
            .queryParam("pair", "btcusdt")
            .get("rest/quotes/snap")
            .then()
            .statusCode(401);
    }

    @Test
    public void RestQuoteSnapRequestDataCorrectness() {

        String pair = "btcusdt";
        QuoteDTO dto = given().auth()
                .oauth2(token)
                .queryParam("pair", pair)
                .when()
                .get("rest/quotes/snap")
                .as(QuoteDTO.class, ObjectMapperType.JACKSON_2);

        Assertions.assertTrue(checkQuotesDTO(dto, pair));

        pair = "ethbtc";
        dto = given().auth()
                .oauth2(token)
                .queryParam("pair", pair)
                .when()
                .get("rest/quotes/snap")
                .as(QuoteDTO.class, ObjectMapperType.JACKSON_2);

        Assertions.assertTrue(checkQuotesDTO(dto, pair));
    }

    @Test
    public void RestQuoteSnapRequestFailing() {

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "btcsdt")
                .get("rest/quotes/snap")
                .then()
                .statusCode(400);

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", "")
                .get("rest/quotes/snap")
                .then()
                .statusCode(400);

        given().auth()
                .oauth2(token)
                .when()
                .get("rest/quotes/snap")
                .then()
                .statusCode(400);

        given().auth()
                .oauth2(token)
                .when()
                .queryParam("pair", (Object) null)
                .get("rest/quotes/snap")
                .then()
                .statusCode(400);
    }


    private boolean checkQuotesDTO(QuoteDTO dto, String pair){

        Assertions.assertEquals(dto.getExchange(), Exchange.NAME);
        Assertions.assertEquals(dto.getMarket(), Exchange.MARKET);
        Assertions.assertEquals(dto.getPair(), pair);

        Set<BigDecimal> asksKeys = dto.getAsks().keySet();
        dto.getBids().keySet().forEach(key -> Assertions.assertFalse(asksKeys.contains(key)));

        dto.getBids().forEach((k, v) -> {
            Assertions.assertTrue(k.compareTo(BigDecimal.ZERO) > 0);
            Assertions.assertTrue(v.compareTo(BigDecimal.ZERO) > 0);
        });

        dto.getAsks().forEach((k, v) -> {
            Assertions.assertTrue(k.compareTo(BigDecimal.ZERO) > 0);
            Assertions.assertTrue(v.compareTo(BigDecimal.ZERO) > 0);
        });

        return true;
    }
}