package com.crypto;

import com.crypto.messages.QuoteDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;


import java.math.BigDecimal;
import java.net.URI;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import static java.util.concurrent.TimeUnit.SECONDS;

@QuarkusTest
public class QuotesStreamTest {

    WebSocketClient socketClient = new ReactorNettyWebSocketClient();
    final CountDownLatch countDownLatch = new CountDownLatch(1);
    ArrayList<QuoteDTO> messages = new ArrayList<>();

    final String url  = "ws://127.0.0.1:8083/";
    final String channel = "quotes/";
    final String pair = "btcusdt";

    @Test
    @Timeout(value = 20, unit = SECONDS)
    public void WebSocketQuotesDataCorrectness() {

        CountDownLatch countDownLatch = new CountDownLatch(1);

        socketClient.execute(URI.create(url + channel + pair),
                session -> session.receive()
                        .limitRequest(10)
                        .doOnError(this::onError)
                        .map(WebSocketMessage::getPayloadAsText)
                        .doOnNext(this::onEvent)
                        .log()
                        .doOnComplete(()-> {
                            Assertions.assertTrue(testTradesCorrectness(messages));
                            countDownLatch.countDown();
                        })
                        .then()).subscribe();

        try {
            countDownLatch.await();
        } catch (InterruptedException e){
            Assertions.fail(e.getMessage());
        }
    }

    private void onError(Throwable e) {
        Assertions.fail("Error while consuming messages: " + e.getMessage());
    }

    private void onEvent(String message) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            QuoteDTO dto = objectMapper.readValue(message, QuoteDTO.class);
            messages.add(dto);
        } catch (JsonProcessingException e) {
            Assertions.fail(e.getMessage());
            e.printStackTrace();
        }
    }

    private boolean testTradesCorrectness(ArrayList<QuoteDTO> messages){

        for (QuoteDTO dto : messages) {

            Assertions.assertEquals(dto.getExchange(), Exchange.NAME);
            Assertions.assertEquals(dto.getMarket(), Exchange.MARKET);
            Assertions.assertEquals(dto.getPair(), pair);
            Assertions.assertNotNull(dto.getAsks());
            Assertions.assertNotNull(dto.getBids());

            dto.getBids().forEach((price, volume) -> {
                Assertions.assertTrue(price.compareTo(BigDecimal.ZERO) > 0);
                Assertions.assertTrue(volume.compareTo(BigDecimal.ZERO) >= 0);
            });

            dto.getAsks().forEach((price, volume) -> {
                Assertions.assertTrue(price.compareTo(BigDecimal.ZERO) > 0);
                Assertions.assertTrue(volume.compareTo(BigDecimal.ZERO) >= 0);
            });
        }

        countDownLatch.countDown();
        return true;
    }
}